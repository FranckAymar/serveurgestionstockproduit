package org.akounda;

import org.akounda.entity.Produit;
import org.akounda.repository.ProduitRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class ServeurGestionStockProduitsApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx =SpringApplication.run(ServeurGestionStockProduitsApplication.class, args);
		
		/*ProduitRepository produitRepository = ctx.getBean(ProduitRepository.class);
		
		produitRepository.save(new Produit("Livre", 50, 200));
		produitRepository.save(new Produit("Cahier", 50, 200));
		produitRepository.save(new Produit("Stylo", 50, 200));
*/
	}
}
