package org.akounda.service;

import java.util.List;

import org.akounda.entity.Produit;
import org.akounda.repository.ProduitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
@Primary
public class ProduitService implements IProduitService {

	@Autowired
	private ProduitRepository produitRepository;
	
	@Override
	public List<Produit> getProduit() {
		return produitRepository.findAll();
	}

	@Override
	public void addProduit(Produit produit) {
		produitRepository.save(produit);
		
	}

	@Override
	public void updateProduit(Produit produit) {
		produitRepository.save(produit);

	}

	@Override
	public void deleteProduit(Long id) {
		Produit produit = new Produit();
		produit.setId(id);
		produitRepository.delete(produit);

	}

}
