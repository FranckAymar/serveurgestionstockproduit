package org.akounda.service;

import java.util.List;

import org.akounda.entity.Produit;

public interface IProduitService {

	List<Produit> getProduit();
	
	void addProduit(Produit produit);
	
	void updateProduit(Produit produit);
	
	void deleteProduit(Long id);
	
	
}
