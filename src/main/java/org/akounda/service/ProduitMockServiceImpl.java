package org.akounda.service;

import java.util.ArrayList;
import java.util.List;

import org.akounda.entity.Produit;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
public class ProduitMockServiceImpl implements IProduitService{

	public ProduitMockServiceImpl(){
		produits = new ArrayList<Produit>();
		
		produits.add(new Produit("Livre", 50, 500));
		produits.add(new Produit("CAhiers", 505, 500));

		
	}
	
	private List<Produit> produits;
	
	@Override
	public List<Produit> getProduit() {
		
		return produits;
	}

	@Override
	public void addProduit(Produit produit) {
		
		produits.add(produit);
		
	}

	@Override
	public void updateProduit(Produit produit) {
		// TODO Auto-generated method stub
		produits.remove(produit);
		produits.add(produit);
	}

	@Override
	public void deleteProduit(Long id) {
		
		Produit produit = new Produit();
		produit.setId(id);
		produits.remove(produit);
		
	}

}
